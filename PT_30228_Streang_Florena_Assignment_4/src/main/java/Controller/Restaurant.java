package Controller;



import Model.BaseProduct;
import Model.CompositeProduct;
import Model.MenuItem;
import Model.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
@Deprecated
public class Restaurant extends Observable implements IRestaurantProcessing {

    private ArrayList<MenuItem> itemArrayList;
    private HashMap<Order,ArrayList<MenuItem>> hashMap;
    private ArrayList<Order> orderArrayList;
    private int nr=0;

    public Restaurant() {
        this.itemArrayList = new ArrayList<>();
        this.orderArrayList = new ArrayList<>();
        this.hashMap=new HashMap<>();
    }

    @Override
    public void createMenuItem(MenuItem item) {

            itemArrayList.add(item);

    }
    
    public DefaultTableModel printMenuItems(JTable jTable){
        String[] column={"Name","Price","Ingredients"};
        DefaultTableModel table=new DefaultTableModel(column,0);
        for(MenuItem item: itemArrayList) {
          if(item.getClass().getSimpleName().equals("BaseProduct"))
            table.addRow(new String[]{item.getName(), String.valueOf(item.computePrice()),"-"});
          else
          {
             String ingredients=((CompositeProduct)item).printIngredients();
             table.addRow(new String[]{item.getName(), String.valueOf(item.computePrice()),ingredients});
             System.out.println(ingredients);

          }

        }
        jTable.setModel(table);
        jTable.setBounds(50,150,400,200);
        return  table;

    }

    public DefaultTableModel printOrder(JTable jTable){
        String[] column={"Id","Date","Table","List","Total"};
        DefaultTableModel table=new DefaultTableModel(column,0);

        for(Order order: orderArrayList) {
            ArrayList<MenuItem> menuItems=hashMap.get(order);
            String product="";
            for (MenuItem item: menuItems
                 ) {
                product = product + item.getName() + ",";

            }
            table.addRow(new String[]{String.valueOf(order.getId()), String.valueOf(order.getDate()), String.valueOf(order.getTable()), product,String.valueOf(computePrice(order))});
        }

        jTable.setModel(table);
        jTable.setBounds(50,350,400,200);
        return table;

    }

    @Override
    public void deleteMenuItem(MenuItem itemm) {
        int nr=0;
        int [] items = new int[100];

        for (int i=0;i<itemArrayList.size();i++) {
            if(itemArrayList.get(i).getName().equals(itemm.getName())) {
                items[nr] = i;
                nr++;
            }
            if(itemArrayList.get(i).getClass().getSimpleName().equals("CompositeProduct"))
                if(((CompositeProduct)itemArrayList.get(i)).searchBaseProduct(itemm)) {
                    items[nr]= i;
                    nr++;
                }

        }
        if(nr==1)
            itemArrayList.remove(items[0]);
        else
        for(int i=0;i<nr-1;i++)
            itemArrayList.remove(items[i]);

    }



    public MenuItem findMenuItem(String name) {
        for (MenuItem m:itemArrayList) {
            if(m.getName().equals(name)){
                return m;
            }
        }
        return null;
    }

    public double getTotal(){
        double total=0;
        for(Order o: orderArrayList){
            total+=computePrice(o);
        }
        System.out.println(total);
        return total;
    }

    @Override
    public void editMenuItem(MenuItem item) {
        for (MenuItem m:itemArrayList) {
            if(m.getName().equals(item.getName())){
                System.out.println("in lista:"+m.getName()+"compara cu"+item.getName());
                m.setPrice(item.computePrice());
            }
        }
    }

    @Override
    public void createOrder(Order o,ArrayList<MenuItem> menuItems) {

        orderArrayList.add(o);
        hashMap.put(o,menuItems);

    }

    @Override
    public double computePrice(Order o) {
        double total=0;
        if(hashMap.containsKey(o)){
            ArrayList<MenuItem> menuItemArrayList=hashMap.get(o);
            for (MenuItem m:menuItemArrayList){
                total+=m.computePrice();
            }

        }
    return total;
    }

    public Order searchOrder(int id,int table){
        for (Order order: orderArrayList) {
            if(order.getId()==id && order.getTable()==table)
                return order;
        }
        return null;
    }

    @Override
    public void generateBill(Order o) throws IOException {
        BillGenerator bill=new BillGenerator(nr);
        nr++;
        bill.generate(o,hashMap,hashMap.get(o),this);

    }

    public static void main(String[] args) throws IOException {
        Order o=new Order(5,6);
        ArrayList<MenuItem> list=new ArrayList<>();
        list.add(new BaseProduct("xola",6));
        Restaurant r=new Restaurant();
        r.createOrder(o,list);
        r.generateBill(o);





    }
}
