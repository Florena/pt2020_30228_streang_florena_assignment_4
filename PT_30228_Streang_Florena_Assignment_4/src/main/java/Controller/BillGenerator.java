package Controller;

import Model.MenuItem;
import Model.Order;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class BillGenerator {
    private final String file_name;

    public BillGenerator(int nr) {
        this.file_name = "Bill"+nr+".txt";
    }
    public String makebill(Order o, ArrayList<MenuItem> arrayList, Restaurant r){
        String ord="Order number:"+o.getId()+"\n";
        for (MenuItem m:arrayList) {
            ord=ord+m.getName()+" "+m.computePrice() + "\n";
        }
        ord=ord+"\nTotal="+r.computePrice(o)+o.toString()+"\n";
        return ord;
    }
    public void generate(Order o, HashMap<Order,ArrayList<MenuItem>>hashMap, ArrayList<MenuItem> itemArrayList,Restaurant r) throws IOException {
        FileWriter file;
        file=new FileWriter(file_name);
        if(hashMap.containsKey(o)){
            ArrayList<MenuItem> items=hashMap.get(o);
            file.write(makebill(o,items,r)+"\n");
        }
        file.close();
    }

}
