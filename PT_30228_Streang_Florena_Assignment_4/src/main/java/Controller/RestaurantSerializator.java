package Controller;

import java.io.*;

public class RestaurantSerializator implements Serializable {
    public static void serialize(Restaurant restaurant) {
        try {
            FileOutputStream file = new FileOutputStream("Restaurant.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(restaurant);
            out.close();
            file.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
    public static Restaurant deserialize() {
        Restaurant restaurant = null;
        try {
            FileInputStream file = new FileInputStream("Restaurant.ser");
            ObjectInputStream in = new ObjectInputStream(file);
            restaurant = (Restaurant) in.readObject();
            in.close();
            file.close();
            return restaurant;
        } catch (IOException e) {
            System.out.println(e);
            restaurant = new Restaurant();
            serialize(restaurant);
            return restaurant;
        } catch (ClassNotFoundException c) {

            System.out.println("Restaurant class not found");
            c.printStackTrace();
            return restaurant;
        }
    }
}
