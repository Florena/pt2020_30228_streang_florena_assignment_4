package Controller;

import Model.MenuItem;
import Model.Order;

import java.io.IOException;
import java.util.ArrayList;

public interface IRestaurantProcessing {



    /**
     * metode pentru administrator
     * @param item
     */

    public void createMenuItem(MenuItem item);
    public void deleteMenuItem(MenuItem item);
    public void editMenuItem(MenuItem item);

    /**
     *pentru ospatar
     * @param o
     * @param menuItems
     */

    public void createOrder(Order o, ArrayList<MenuItem> menuItems);
    public double computePrice(Order o);
    public void generateBill(Order o) throws IOException;
}
