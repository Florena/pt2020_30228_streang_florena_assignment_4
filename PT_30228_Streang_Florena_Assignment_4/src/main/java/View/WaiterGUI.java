package View;

import Model.MenuItem;
import Model.Order;
import Controller.Restaurant;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class WaiterGUI {

    public WaiterGUI(Restaurant restaurant){

            JFrame jFrame=new JFrame("Waiter Interface");
            jFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
            jFrame.setSize(950,500);
            ImageIcon img=new ImageIcon("waiter.png");
            jFrame.setIconImage(img.getImage());
            JLabel label=new JLabel("Order");



            jFrame.getContentPane().add(createContent(restaurant));
            jFrame.add(createContent(restaurant));
            jFrame.setVisible(true);


    }
    private Component createContent(Restaurant restaurant) {

        JPanel panel = new JPanel() ;

        panel.setLayout(null);
        JLabel label=new JLabel("Menu Item :");
        label.setBounds(30,90,90,20);
        panel.add(label);

        JLabel label2=new JLabel("ID Order :");
        label2.setBounds(30,30,90,20);
        panel.add(label2);

        JLabel label3=new JLabel("Table nr :");
        label3.setBounds(30,60,90,20);
        panel.add(label3);

        JTextField textField=new JTextField();
        textField.setBounds(100,30,50,20);
        JTextField textField2=new JTextField();
        textField2.setBounds(100,60,50,20);
        JTextArea textArea=new JTextArea();
        textArea.setBounds(100,90,120,150);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        textArea.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(0,0,0,0)));
        textArea.setVisible(true);
        panel.add(textField);
        panel.add(textField2);
        panel.add(textArea);


        JButton button = new JButton("CREATE");

        button.setAlignmentX(Component.LEFT_ALIGNMENT);
        button.setBounds(50,250,90,20);
        panel.add(button);

        JButton button2 = new JButton("TOTAL");
        button2.setAlignmentX(Component.LEFT_ALIGNMENT);
        button2.setBounds(150,250,90,20);
        panel.add(button2);

        JButton button3 = new JButton("BILL");
        button3.setAlignmentX(Component.LEFT_ALIGNMENT);
        button3.setBounds(250,250,90,20);
        panel.add(button3);
        JTable table2=new JTable();
        ArrayList<String> strings=new ArrayList<>();

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String id=textField.getText();
                String tableno=textField2.getText();
                Order o=new Order(Integer.parseInt(id),Integer.parseInt(tableno));
                ArrayList<MenuItem> list=new ArrayList<>();

                for (String line:textArea.getText().split("\n")
                     ) {
                    MenuItem menuItem=restaurant.findMenuItem(line);
                   if(menuItem!=null)
                    list.add(menuItem);

                }
                restaurant.createOrder(o,list);
                table2.setModel(restaurant.printOrder(table2));
                table2.getTableHeader().setBounds(500,10,400,20);
                table2.setBounds(500,30,400,400);
                panel.add(table2.getTableHeader());
                panel.add(table2);
                panel.revalidate();
                panel.repaint();

            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            JLabel jl=new JLabel("Total ="+String.valueOf(restaurant.getTotal()));
            jl.setBounds(400, 30,120, 20);
            jl.setVisible(true);
            panel.add(jl);
            panel.revalidate();
            panel.repaint();
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String id=textField.getText();
                String tableno=textField2.getText();
                try {
                    restaurant.generateBill(restaurant.searchOrder(Integer.parseInt(id),Integer.parseInt(tableno)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String infoMessage="Bill created";
                String titleBar="Bill generator";
                JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);


            }
        });

        panel.setPreferredSize(new Dimension(200, 200));
        panel.setBackground(Color.WHITE);
        return panel;

    }
}
