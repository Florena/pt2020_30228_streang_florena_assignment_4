package View;

import Controller.Restaurant;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends JFrame implements Observer {
    @Override
    public void update(Observable observable, Object o) {

        this.setVisible(true);
        System.out.println(o.toString());
        int x = JOptionPane.showConfirmDialog(null, 1, "New Order !", 2);
        if (x == 0) {
            System.out.println("Chef will cook!");
            this.setVisible(true);
        } else {
            System.out.println("Chef is busy");
            this.setVisible(true);
        }
    }
    public ChefGUI(Restaurant restaurant) {

        restaurant.addObserver(this);
        JFrame jFrame=new JFrame("Chef Interface");
        jFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        jFrame.setSize(650,500);
        ImageIcon img=new ImageIcon("chef-s-uniform-icon-chef.jpg");
        jFrame.setIconImage(img.getImage());
        jFrame.getContentPane().add(createContent(restaurant));
        jFrame.add(createContent(restaurant));
        jFrame.setVisible(true);

    }

    private Component createContent(Restaurant restaurant) {
        final Image image = requestImage();

        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 20, 20, null);
            }
        };


        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));



        Font biggerFont = new Font("Times New Roman", Font.PLAIN, 18);
        Font hugeFont = new Font("Times New Roman", Font.PLAIN, 32);
        JLabel titleLabel;
        titleLabel = new JLabel("Chef operations !");
        titleLabel.setFont(hugeFont);
        titleLabel.setBounds(300, 50, 450, 50);
        panel.add(titleLabel);

        panel.setPreferredSize(new Dimension(500, 500));
        panel.setBackground(Color.WHITE);
        return panel;

    }
    private Image requestImage(){
        Image im = null;
        try {
            im = ImageIO.read(new File("chef-s-uniform-icon-chef.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return im;
    }




}

