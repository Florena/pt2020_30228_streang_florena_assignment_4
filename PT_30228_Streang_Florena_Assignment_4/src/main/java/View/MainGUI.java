package View;

import Controller.Restaurant;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class MainGUI extends JPanel {

    public MainGUI(){
        JFrame jFrame=new JFrame("User Interface");
        jFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        jFrame.setSize(650,500);
        ImageIcon img=new ImageIcon("user.png");
        jFrame.setIconImage(img.getImage());

        final BufferedImage bg;



        jFrame.getContentPane().add(createContent());
        jFrame.add(createContent());
        jFrame.setVisible(true);

    }
    private Component createContent() {
        final Image image = requestImage();

        Restaurant restaurant=new Restaurant();
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 20, 20, null);
            }
        };


        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

            JButton button = new JButton("ADMIN");
            button.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(Box.createRigidArea(new Dimension(5, 15)));
            panel.add(button);

            JButton button2 = new JButton("WAITER");
            button2.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(Box.createRigidArea(new Dimension(5, 15)));
            panel.add(button2);

            JButton button3 = new JButton("CHEF");
            button3.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(Box.createRigidArea(new Dimension(5, 15)));
            panel.add(button3);

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    AdminGUI ag=new AdminGUI(restaurant);
                }
            });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                WaiterGUI wg=new WaiterGUI(restaurant);
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                ChefGUI cg=new ChefGUI(restaurant);
            }
        });

        panel.setPreferredSize(new Dimension(500, 500));
        panel.setBackground(Color.WHITE);
        return panel;

    }
        private Image requestImage(){
            Image im = null;
            try {
                im = ImageIO.read(new File("unnamed.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return im;
        }



}


