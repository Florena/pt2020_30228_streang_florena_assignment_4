package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

import Model.BaseProduct;
import Model.CompositeProduct;
import Model.MenuItem;
import Controller.Restaurant;

public class AdminGUI {

        public AdminGUI(Restaurant restaurant){
            JFrame jFrame=new JFrame("Admin Interface");
            jFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
            jFrame.setSize(500,500);

            ImageIcon img=new ImageIcon("admin-icon.png");
            jFrame.setIconImage(img.getImage());
            JLabel label=new JLabel("Menu Item");

           jFrame.getContentPane().add(createContent(restaurant));
            jFrame.add(createContent(restaurant));
            jFrame.setVisible(true);

        }

        private Component createContent(Restaurant restaurant) {

            JPanel panel = new JPanel();

            panel.setLayout(null);
            JLabel label = new JLabel("Menu Item");
            label.setBounds(30, 30, 90, 50);
            panel.add(label);


            JLabel label3 = new JLabel("$");
            label3.setBounds(220, 40, 50, 20);
            panel.add(label3);

            JTextField textField = new JTextField();
            textField.setBounds(100, 40, 70, 20);
            JTextField textField2 = new JTextField();
            textField2.setBounds(170, 40, 50, 20);
            panel.add(textField);
            panel.add(textField2);


            JButton button = new JButton("Create");

            button.setAlignmentX(Component.LEFT_ALIGNMENT);
            button.setBounds(50, 100, 90, 20);
            panel.add(button);

            JButton button2 = new JButton("Delete");
            button2.setAlignmentX(Component.LEFT_ALIGNMENT);
            button2.setBounds(150, 100, 90, 20);
            panel.add(button2);

            JButton button3 = new JButton("Edit");
            button3.setAlignmentX(Component.LEFT_ALIGNMENT);
            button3.setBounds(250, 100, 90, 20);
            panel.add(button3);

            JButton button4 = new JButton("Make");
            button4.setAlignmentX(Component.LEFT_ALIGNMENT);
            button4.setBounds(350, 100, 90, 20);
            panel.add(button4);
            JTable table = new JTable();


            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String name = textField.getText();
                    String price = textField2.getText();
                    double p = Double.parseDouble(price);
                    BaseProduct menuItem = new BaseProduct(name, p);
                    restaurant.createMenuItem(menuItem);
                    restaurant.printMenuItems(table);
                    table.getTableHeader().setBounds(50, 130, 400, 20);
                    panel.add(table.getTableHeader());
                    panel.add(table);
                    panel.repaint();


                }
            });

            button2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String name = textField.getText();
                    String price = textField2.getText();
                    double p = Double.parseDouble(price);
                    BaseProduct menuItem = new BaseProduct(name, p);
                    restaurant.deleteMenuItem(menuItem);
                    table.setModel(restaurant.printMenuItems(table));
                    table.getTableHeader().setBounds(50, 130, 400, 20);

                    panel.add(table.getTableHeader());
                    panel.add(table);
                    panel.revalidate();
                    panel.repaint();


                }
            });

            button3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String name = textField.getText();
                    String price = textField2.getText();
                    double p = Double.parseDouble(price);
                    BaseProduct menuItem = new BaseProduct(name, p);
                    restaurant.editMenuItem(menuItem);
                    table.setModel(restaurant.printMenuItems(table));
                    table.getTableHeader().setBounds(50, 130, 400, 20);

                    panel.add(table.getTableHeader());
                    panel.add(table);
                    panel.revalidate();
                    panel.repaint();


                }
            });
            button4.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    String name = null;
                    String namecp = textField.getText();
                    ///String price = textField2.getText();
                    ArrayList<MenuItem> ingredients=new ArrayList<>();
                    int[] selectedRow = table.getSelectedRows();
                    int[] selectedColumns = table.getSelectedColumns();
                    for (int i = 0; i < selectedRow.length; i++) {
                        for (int j = 0; j < selectedColumns.length; j++) {
                            name = (String) table.getValueAt(selectedRow[i], selectedColumns[j]);
                            ingredients.add(restaurant.findMenuItem(name));
                        }
                    }
                    CompositeProduct product=new CompositeProduct(namecp,0,ingredients);
                    restaurant.createMenuItem(product);
                    restaurant.printMenuItems(table);
                    table.getTableHeader().setBounds(50, 130, 400, 20);
                    panel.add(table.getTableHeader());
                    panel.add(table);
                    panel.repaint();
                }
            });

            panel.setPreferredSize(new Dimension(200, 200));
            panel.setBackground(Color.WHITE);
            return panel;

        }




    }




