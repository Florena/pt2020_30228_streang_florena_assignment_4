package Model;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    private ArrayList<MenuItem> ingredients;

    public CompositeProduct(String name, double price,ArrayList<MenuItem> ingredients) {
        super(name, price);
        this.ingredients=ingredients;
    }


    public String printIngredients() {
        String string="";
        for (MenuItem m:ingredients
             ) {
            string=string+m.getName()+" ";
        }
        return string;
    }

    public ArrayList<MenuItem> getIngredients() {
        return ingredients;
    }

    public void deleteIngredients(MenuItem ingredient) {
        for (MenuItem m : ingredients
        ) {
            if (m.getName().equals(ingredient.getName()))
                ingredients.remove(m);
        }
    }

    public boolean searchBaseProduct(MenuItem menuItem) {
        for (MenuItem m : ingredients) {
            if (m.getName().equals(menuItem.getName())) {
                return true;
            }

        }
        return false;
    }

    public void setIngredients(MenuItem ingredient){
        for (MenuItem m : ingredients) {
            if (m.getName().equals(ingredient.getName()))
                m.setPrice(ingredient.getPrice());
        }
    }

    @Override
    public double computePrice() {
        double price=0;
        for (MenuItem m:ingredients) {
            price+=m.computePrice();
        }
        return price;
    }
}
