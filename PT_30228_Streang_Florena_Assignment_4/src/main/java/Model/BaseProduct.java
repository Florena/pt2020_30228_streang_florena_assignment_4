package Model;

public class BaseProduct extends MenuItem {

    public BaseProduct(String name, double price)
    {
        super(name,price);
    }

    @Override
    public double computePrice() {
        return getPrice();
    }

}
