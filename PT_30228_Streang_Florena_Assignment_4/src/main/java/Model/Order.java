package Model;

import java.util.Date;
import java.util.Objects;
import java.util.Random;

public class Order {
    public int id;
    public Date date;
    public int table;


    public  Order(int id,int table){
        Random rnd = new Random();
        Date date = new Date((System.currentTimeMillis() - rnd.nextLong()));
        this.id=id;
        this.date=date;
        this.table=table;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return id == order.id &&
                table == order.table &&
                Objects.equals(date, order.date);
    }

    @Override
    @Deprecated
    public int hashCode() {
        int hashcode =11;
        hashcode=id *31+table*5+ date.getDay();

        return hashcode;
    }
    @Override
    public String toString() {
        return "Order no" +
                "=" + id +
                "\ndate=" + date +
                "\ntable=" + table +
                '\n';
    }

    public static void main(String[] args) {
        Order o=new Order(2,3);
        System.out.println(o.toString());
    }
}
