package Model;

public abstract class MenuItem {
    private String name;
    private double price;

    public MenuItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setNume(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract double computePrice();

    @Override
    public String toString() {
        return
                "Name =" + name +
                "\t price=" + price +
                '\n';
    }

}
